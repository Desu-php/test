<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

function debug($str){
    echo '<pre>';
    var_dump($str);
    echo '</pre>';
    exit();
}

function session_one($key){
    $status = '';
    if (isset($_SESSION[$key])){
        $status = $_SESSION[$key];
        unset($_SESSION[$key]);
    }
    if (!empty(trim($status))){
        $status = '<script>alert("'.$status.'")</script>';
    }
    return $status;
}
?>