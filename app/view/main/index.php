
<div class="modal fade" id="addList" tabindex="-1" role="dialog" aria-labelledby="addListLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addListLabel">Добавление задания</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?=$uri;?>/add" class="text-center" method="post">
                    <input type="text" class="form-control mb-3" required placeholder="Имя" name="name">
                    <input type="email" class="form-control mb-3" required placeholder="email" name="email">
                    <textarea name="text" class="form-control mb-3" required id="" placeholder="описание" cols="10" rows="5"></textarea>
                    <input type="submit" name="addTask" class="btn btn-primary" value="Добавить">
                </form>
            </div>

        </div>
    </div>
</div>
<header class="mb-3">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <button class="btn btn-primary "  data-toggle="modal" data-target="#addList">Добавить</button>
                </li>
                <li class="nav-item">
                    <div class="dropdown ml-2">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Фильтр
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?=$uri;?>/main/index/<?=$page?>?order=name&type=ASC">имя по возрастанию</a>
                            <a class="dropdown-item" href="<?=$uri;?>/main/index/<?=$page?>?order=name&type=DESC">имя по убыванию</a>
                            <a class="dropdown-item" href="<?=$uri;?>/main/index/<?=$page?>?order=email&type=ASC">email по возрастанию</a>
                            <a class="dropdown-item" href="<?=$uri;?>/main/index/<?=$page?>?order=email&type=DESC">email по убыванию</a>
                            <a class="dropdown-item" href="<?=$uri;?>/main/index/<?=$page?>?order=name_status&type=ASC">статус по возрастанию</a>
                            <a class="dropdown-item" href="<?=$uri;?>/main/index/<?=$page?>?order=name_status&type=DESC">статус по убыванию</a>
                        </div>
                    </div>
                </li>
            </ul>
            <?php if(!isset($_SESSION['admin'])): ?>
            <button type="button" class="btn btn-primary my-2 my-lg-0" data-toggle="modal" data-target="#exampleModal">
                Войти
            </button>
            <?php else: ?>
            <a href="<?=$uri;?>/logout" class="btn btn-primary my-2 my-lg-0" >Выйти</a>
            <?php endif; ?>
        </div>
    </nav>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Окошко авторизации</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?=$uri;?>/login" class="text-center" method="post">
                        <input type="text" required class="form-control mb-3" placeholder="login" name="login">
                        <input type="password" required class="form-control mb-3" placeholder="password" name="password">
                        <input type="submit" class="btn btn-primary" value="авторизоваться" name="auth">
                    </form>
                </div>

            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="lists">
        <?php foreach ($tasks as $task):  ?>
        <div class="card text-center mb-3">
            <div class="card-header">
                <span class="text-left"><?=htmlspecialchars($task['name'])?></span>
                <br>
                <span class="text-secondary"><?=htmlspecialchars($task['email'])?></span>
            </div>
            <div class="card-body">
                <p class="card-text"><?=htmlspecialchars($task['text'])?></p>
                <p style="position: absolute; right:5%;bottom:0px;" class="text-primary"><?=$task['change']?></p>
                <div class="modal fade" id="change<?=$task['id_task']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Окошко редактирования</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="<?=$uri;?>/main/edit/<?=$task['id_task']?>" class="text-center" method="post">
                                    <textarea name="text" class="form-control mb-3" required id="" placeholder="описание" cols="10" rows="5"><?=htmlspecialchars($task['text'])?></textarea>
                                    <select class="form-control" name="status">
                                        <option value="<?=$task['id_status']?>"><?=$task['name_status']?></option>
                                        <?php foreach ($select_status as $item): ?>
                                            <?php if ($item['id_status'] != $task['id_status'] ): ?>
                                            <option value="<?=$item['id_status']?>"><?=$item['name_status']?></option>
                                            <?php endif; ?>
                                        <?php endforeach;  ?>
                                    </select>
                                    <input type="submit" class="btn btn-primary" value="изменить" name="edit">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (isset($_SESSION['admin'])): ?>
                <a href="#"   class="btn btn-primary "  data-toggle="modal" data-target="#change<?=$task['id_task']?>">Редактировать</a>
                <?php endif; ?>
            </div>
            <div class="card-footer text-muted">
                <?=$task['name_status']?>
            </div>
        </div>
       <?php endforeach; ?>
    </div>
</div>
<div class="pagination justify-content-center">
    <nav aria-label="Page navigation example">
            <?=$pagination?>
    </nav>
</div>
<?=$status?>