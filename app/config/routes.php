<?php

return [

        '' => [
            'controller' => 'main',
            'action' => 'index'
            ],

        'add'=> [
            'controller' => 'main',
            'action' => 'addTask',
        ],

        'main/index/{page:\d+}' => [
            'controller' => 'main',
            'action' => 'index'
        ],

    'login' => [
        'controller' => 'main',
        'action' => 'login'
    ],
    'logout' => [
        'controller' => 'main',
        'action' => 'logout'
    ],

    'main/edit/{page:\d+}' => [
        'controller' => 'main',
        'action' => 'edit'
    ],

];