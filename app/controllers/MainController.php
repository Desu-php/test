<?php

namespace app\controllers;
use app\core\Controller;
use app\core\View;
use app\lib\Pagination;
use app\models\Main;

class MainController extends Controller
{

    protected $error;

    public function indexAction(){
        $uri = $this->getUri();
        if (!isset($this->route['page'])){
            $page = 1;
        }else{
            $page = $this->route['page'];
        }
        if (isset($_GET['order']) and isset($_GET['type'])){
            $order =  $this->orderFilter($_GET['order']);
            $type = $this->typeFilter($_GET['type']);
            $tasks =  $this->model->getTasks($page,$order, $type);
        }else{
            $tasks =  $this->model->getTasks($page);
        }
        $status  = session_one('status');
        $select_status = $this->model->getStatus();
        $page = 1;
        $pagination = new Pagination($this->route, $this->model->tasksCount(), 3);
        if (isset($this->route['page'])){
            $page = $this->route['page'];
        }
        $vars = [
            'pagination' => $pagination->get(),
            'tasks' => $tasks,
            'status' => $status,
            'select_status' => $select_status,
            'page' => $page,
            'uri' => $uri,
        ];
        View::Render('main/index', $vars);
    }

    public function addTaskAction(){
        $model = new Main();
        $Response = '';
        if (isset($_POST['addTask'])){
           if (!$this->taskValidate($_POST)) {
               $Response = $this->error;
           }else{
               $model->addTask($_POST);
               $Response = 'Вы успешно добавили задачу';
           }
        }
        View::redirect('/', $Response);
    }

    public function taskValidate($post){
        $len = iconv_strlen(trim($post['name']));
        $textLen = iconv_strlen(trim($post['text']));
        if ($len <= 0){
            $this->error = 'Вы не написали свое имя';
            return false;
        }elseif (!filter_var($post['email'],FILTER_VALIDATE_EMAIL)){
            $this->error = 'Email указан не верно';
            return false;
        }elseif ($textLen <= 0){
            $this->error = 'Описание задачи не может быть пустым';
            return false;
        }
        return true;
    }

    public function loginAction()
    {
        if (isset($_SESSION['admin'])) {
            debug($_SESSION);
            View::redirect('/');
        }
        if (isset($_POST['auth'])) {

            if (!$this->loginValidate($_POST)) {
               View::redirect('/', $this->error);
            }
            $_SESSION['admin'] = true;
            View::redirect('/');
        }
        View::redirect('/');
    }

    public function loginValidate($post){
        $config = require 'app/config/admin.php';
        if ($config['login'] != $post['login'] or $config['password'] != $post['password']){
            $this->error = 'Логин или пароль указан неверно';
            return false;
        }
        return true;
    }

    public function logoutAction(){
        if (isset($_SESSION['admin'])){
            unset($_SESSION['admin']);
            View::redirect('/');
        }
        View::redirect('/');
    }

    public function editAction(){
        if (isset($_SESSION['admin'])){
            if (isset($_POST['edit'])){
          $request =  $this->model->getTask($this->route);
          if ($request[0]['text'] == $_POST['text']){
              $this->model->updateTask($_POST,$this->route,$request[0]['change']);
          }else{
              $this->model->updateTask($_POST,$this->route,'отредактировано администратором');
          }
          View::redirect('/');
        }
        }else{
            View::redirect('/','Пожалуйста авторизуйтесь');
        }
    }

    public function orderFilter($order){
        switch ($order){
            case 'name':
                return $order;
            case 'email':
                return $order;
            case 'name_status':
                return $order;
        }

    }

    public function typeFilter($type){
        switch ($type){
            case 'ASC':
                return $type;
            case 'DESC':
                return $type;
        }
    }
}