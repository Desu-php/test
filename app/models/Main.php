<?php

namespace app\models;
use app\core\Model;

class Main extends Model
{
    public function addTask($post){
        $params  = [
            'id_task' => '',
            'name' => $post['name'],
            'email' => $post['email'],
            'text' => $post['text'],
        ];
        $this->db->query('INSERT INTO tasks VALUES (:id_task,:name, :email, :text, 1, "" )',$params);
    }

    public function getTasks($page,$order = 'id_task', $type = 'DESC'){
        $max = 3;
        $params = [
            'max' => $max,
            'start' => ($page - 1) * $max,
        ];
        return $this->db->row('SELECT * FROM tasks, status WHERE tasks.id_status = status.id_status 
                                   ORDER BY '.$order.' '.$type.' LIMIT :start, :max',$params);
    }

    function tasksCount(){
        return $this->db->column('SELECT COUNT(id_task) FROM tasks');
    }

    public function getStatus(){
        return $this->db->row('SELECT * FROM status');
    }

    public function updateTask($post,$route,$change = ''){
        $params = [
            'text' => $post['text'],
            'id_status' => $post['status'],
            'change' => $change,
            'id_task' => $route['page'],

        ];
        return $this->db->query('Update tasks SET text = :text, id_status = :id_status, tasks.change = :change
                                     WHERE id_task = :id_task', $params);
    }

    public function getTask($route){
        $params = [
           'id_task' => $route['page']
        ];
      return $this->db->row('SELECT * FROM tasks WHERE id_task = :id_task',$params);
    }
}