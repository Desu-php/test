<?php

namespace app\core;

class View
{

    public  static function Render($content,array $data = [], $template = 'default'){
        $content = 'app/view/'.$content.'.php';
       $path =  'app/view/layouts/'.$template.'.php';
       if (!file_exists($path)){
           exit('Вид не найден!');
       }
        if(is_array($data)) {
            extract($data);
        }
        include $path;
    }

    public static function errorCode($code){
        http_response_code($code);
        $path = 'app/view/errors/'.$code.'.php';
        if (!file_exists($path)) exit('Файл не найден');
        require $path;
        exit();
    }

    public static function  redirect($url, $status = ''){
        $_SESSION['status'] = $status;
        header('location: '.$url);
        exit();
    }
}