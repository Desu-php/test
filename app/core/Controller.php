<?php

namespace app\core;
use app\core\View;

abstract class Controller
{

    public $route;

    public function __construct($route)
    {
        $this->route = $route;
        $this->model = $this->loadModel($route['controller']);
    }

    public function loadModel($name){
        $path = 'app\models\\'.ucfirst($name);
        if (class_exists($path)){
            return new $path;
        }
        return false;

    }

    protected function getUri(){
        $http = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'? 'https' : 'http';
        if($_SERVER["SERVER_PORT"] == 443)
            $http = 'https';
        elseif (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1')))
            $http = 'https';
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
            $http = 'https';
        $theURI = $http.'://'.$_SERVER['SERVER_NAME'];
        $theURI = str_replace(["'",'"', '<','>'],["%27","%22","%3C","%3E"],$theURI);
        return $theURI;
    }
}

